//general interface
#include <Arduino.h>
#include <SPI.h>
#include <Ethernet.h>
// #include <ArduinoOTA.h>
#include <config.h>
// #include <MemoryFree.h>



// To use the TCP version of rosserial_arduino
#define ROSSERIAL_ARDUINO_TCP

//ROS
#include <ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/UInt16.h>
// #include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Empty.h>
#include <rosserial_arduino/Test.h>

//power interface
#include <Motor.h>
#include <pinout.h>

//Feedback interface
#include <Wire.h>
#include <I2CEncoder.h> //https://github.com/alexhenning/I2CEncoder?fbclid=IwAR1FRFiVtNuNwpy7uDR8T3gacyD7tOC_vkBq7BlZtldRjwZJgsAbo6WTmyw

////////////////////////////////////////Ethernet/////////////////////////////////
// Set the shield settings
uint16_t node_speed = 1/50; //ms
uint32_t last_time = 0;
////////////////////////////////////////////////////////////////////////////////

//motor control pins
//digital pins can be any digital pins
//PWM: 2 to 13 and 44 to 46. Provide 8-bit PWM output with the analogWrite() function.
//for documentation refer to the https://store.arduino.cc/usa/mega-2560-r3

//Project details: http://24.86.84.132:8090/pages/viewpage.action?pageId=7274634

/*PINOUT 
MOTOR CONTROLS
(outside of the enclosure view, left-to-right)
(inside view is right-to-left)
1---------------------------------------------------10: 
5V, PWM11, IN11, IN12, GND, GND, IN13, IN14, PWM12, 5V

11--------------------------------------------------20:
5V, PWM21, IN21, IN22, GND, GND, IN23, IN24, PWM22, 5V

ENCODERS FRONT/BACK
(outside of the enclosure view, left-to-right)
(inside view is right-to-left)
9-------------------------------------16:
5V, SDA3, SCL3, GND, GND, SDA4, SCL4, 5V

1-------------------------------------8: 
5V, SDA1, SCL1, GND, GND, SDA2, SCL2, 5V
 */

//motor variables + encoders (encoders use interrupt pins)
//Motor 1 - FR; Motor 2 - FL; Motor 3 - RL, Motor 4 - RR
float FLmotorSpeed = 0;
float FRmotorSpeed = 0;
float BLmotorSpeed = 0;
float BRmotorSpeed = 0;

/*
IF USING iANTS_DCU_mk.1, pinout is the following:
Updated pinout:
Bottom row (FRONT):
PWM3 = PWM4
IN11 = 31
IN12 = 33
PWM4 = PWM3
IN13 = 30
IN14 = 32

Top row (BACK):
PWM5 =  PWM5
IN21 = 34
IN22 = 36

PWM6 = PWM6
IN23 = 35
IN24 = 37
*/

Motor FLmotor = Motor(FLmotorIN1, FLmotorIN2, FLmotorPWM); //FL
Motor FRmotor = Motor(FRmotorIN1, FRmotorIN2, FRmotorPWM); //FR
Motor BLmotor = Motor(BLmotorIN1, BLmotorIN2, BLmotorPWM); //BL
Motor BRmotor = Motor(BRmotorIN1, BRmotorIN2, BRmotorPWM); //BR

//Encoder data
I2CEncoder* encoderList = new I2CEncoder[8]; //library for the 8-bit vex encoders
long* encoderFeedback = new long[8]; //encoder feedback from encoders uses LONG for the data type
long* encoderFeedbackPreviousState = new long[8];
int numberOfEncoderOverflows = 0;
int64_t* encoderValueToROS = new int64_t[8]; //these two arrays hold large numbers needed for the proper operation of the final feedback

//Motor debugger:
// String rosString = "";
// char* speed = new char[10];

//DCU specific function
void initializeEncoders();
void getFeedbackFromMotors();
void publishEncoderValuesToROS();
void publishWheelFeedbackToROS();
void FRwheel(const std_msgs::Float64& msg1);
void FLwheel(const std_msgs::Float64& msg2);
void BRwheel(const std_msgs::Float64& msg3);
void BLwheel(const std_msgs::Float64& msg4);
void moving();
void feedbackPIDMode();
void postFeedbackToROS();
void unlockPowerToMotors(const std_msgs::UInt16& cmd_msg);
void tcaselect(uint8_t i);

////////////////////////////////////////////////ROS ////////////////////////////////////////////////////////////////////////////
//HANDLER
ros::NodeHandle DCU;
// using rosserial_arduino::Test;

// int i;
// void callback(const Test::Request & req, Test::Response & res){
//   if((i++)%2)
//     res.output = "hello";
//   else
//     res.output = "world";
// }

// ros::ServiceServer<Test::Request, Test::Response> server("test_srv",&callback);
char warn[] = "Connected!";
// String debugLine = "";

//DRIVE & POWERTRAIN -> speed is in rad/s
//NOTE: USE EITHER DIRECT COMMAND VELOCITY OR PID BASED CONTROL EFFORT
float maxAngularVelocity = 39.27; //39.27 rad/s max speed -> 1.5 m/s -> 29 rad/s with 2 inch wheel diameter -> 1.33 gear reduction

//direct values from cmd_vel
ros::Subscriber<std_msgs::Float64> FRwheelVelocity("/base/fr_wheel/cmd_vel", FRwheel); //FR wheel 
ros::Subscriber<std_msgs::Float64> FLwheelVelocity("/base/fl_wheel/cmd_vel", FLwheel); //FL wheel
ros::Subscriber<std_msgs::Float64> BRwheelVelocity("/base/br_wheel/cmd_vel", BRwheel); //BR wheel 
ros::Subscriber<std_msgs::Float64> BLwheelVelocity("/base/bl_wheel/cmd_vel", BLwheel); //BL wheel 

//TEST TOPIC - RE-WRITE IT MANUALLY TO TIGHT IT TO THE CERTAIN TOPIC
//ros::Subscriber<std_msgs::Float64> wheelTrigger("/base/bl_wheel/cmd_vel", BLwheel); //change wheel here to BL, BR, FR, FL

//contactor power
ros::Subscriber<std_msgs::UInt16> contactorRelay("/base/contactor", unlockPowerToMotors);

//publish
// std_msgs::String powerLocker_msg;
// ros::Publisher motor_power("motor_debug", &powerLocker_msg);

//encoders to ROS support
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Front Encoders (REFER TO THE ALTIUM SCHEMATIC FOR DETAILS!!!!):
Encoder1 -> Front Left Top                                      Encoder4 -> Front Right Top
Encoder2 -> Front Left Bottom                                   Encoder3 -> Front Right Bottom

Rear Encoders:
Encoder5 -> Rear Left Top                                       Encoder8 -> Rear Right Top
Encoder6 -> Rear Left Bottom                                    Encoder7 -> Rear Right Bottom
*/ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

std_msgs::Int64 FR_msg; //Front Right top wheel
ros::Publisher FR_debug("/base/fr_wheel/encoder", &FR_msg);

std_msgs::Int64 FL_msg; //Front Right top wheel
ros::Publisher FL_debug("/base/fl_wheel/encoder", &FL_msg);

std_msgs::Int64 BR_msg; //Front Right top wheel
ros::Publisher BR_debug("/base/br_wheel/encoder", &BR_msg);

std_msgs::Int64 BL_msg; //Front Right top wheel
ros::Publisher BL_debug("/base/bl_wheel/encoder", &BL_msg);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//MAIN
void setup() {
  // Use serial to monitor the process
  Serial.begin(9600);
  Wire.begin();
  // Connect the Ethernet
  Ethernet.init(10);
  Ethernet.begin(mac, ip);
  // Let some time for the Ethernet Shield to be initialized
  delay(1000);

  Serial.println("");
  Serial.println("MCU Ethernet connected");
  Serial.println("IP address: ");
  Serial.println(Ethernet.localIP());

  // Set the connection to rosserial socket server
  DCU.getHardware()->setConnection(server_ip, serverPort);
  //ros
  DCU.initNode();

  // Another way to get IP
  Serial.print("IP = ");
  Serial.println(DCU.getHardware()->getLocalIP());

  //motor subscriptions allows us to read motor values
  DCU.subscribe(FRwheelVelocity);
  DCU.subscribe(FLwheelVelocity);
  DCU.subscribe(BRwheelVelocity);
  DCU.subscribe(BLwheelVelocity);
  //DCU.subscribe(wheelTrigger); //universal wheel tester for Float64 speed tests

  //contactor sub
  DCU.subscribe(contactorRelay);

  //contactor pub
  // DCU.advertise(motor_power);

  //encoder pub
  DCU.advertise(FR_debug);
  DCU.advertise(FL_debug);
  DCU.advertise(BR_debug);
  DCU.advertise(BL_debug);

  // DCU.advertiseService(server);

  //INITIALIZATION OF HARDWARE
  initializeEncoders(); //constant operation
  pinMode(powerLocker, OUTPUT);

  // start the OTEthernet library with internal (flash) based storage
  // ArduinoOTA.begin(Ethernet.localIP(), "Arduino", "password", InternalStorage);
}

void loop() {
  // if(millis() - last_time >= node_speed) {
  //   last_time = millis();

  //   if (DCU.connected()) {
  //     Serial.println("Connected");
  //     // Serial.print("Runnning FreeMemory=");
  //     // Serial.println(freeMemory());
  //     // Serial.println(millis());

  //     // topic publishers
  //     motor_power.publish( &powerLocker_msg ); //power locker, str_msg.data is generated based on the input from the ROS power locker 

  //     getFeedbackFromMotors(); //get feedback from all available encoders
  //     publishEncoderValuesToROS(); //publish values to ROS

  //     //execute wheel movement
  //     moving();
  //     DCU.logwarn(warn);
  //     ig = ig +1;
  //     } else {
  //       // Serial.println("Not Connected");
  //       Serial.print("Not Runnning FreeMemory=");
  //       Serial.println(freeMemory());
  //     }
  // }
  // //node handling
  // DCU.spinOnce();
  // ArduinoOTA.poll();
  // delay(1);

  while(true){
    if(!DCU.connected()){
      Serial.println("Not Connected");
    }
    // motor_power.publish( &powerLocker_msg ); //power locker, str_msg.data is generated based on the input from the ROS power locker 
    getFeedbackFromMotors(); //get feedback from all available encoders
    publishEncoderValuesToROS(); //publish values to ROS

    //execute wheel movement
    moving();
    DCU.logwarn(warn);

    DCU.spinOnce();
    // ArduinoOTA.poll();
    delay(node_speed);
  }
}

/////////////////////////////////////////////////////////////FUNCTIONS //////////////////////////////////////////////////////////////////////////////////////////////
void tcaselect(uint8_t i) { //8 encoders multiplexing
  if (i > 7) return;
 
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();  
}

void initializeEncoders() {
  Wire.begin();

  for(int i = 0; i < 8; i++) {
    tcaselect(i); //INSERT YOUR NUMBER HERE IF YOU WOULD LIKE TO REQUEST A CERTAIN ENCODER INSTEAD OF ALL OF THEM
    encoderList[i].init(MOTOR_393_TORQUE_ROTATIONS, MOTOR_393_TIME_DELTA);
    encoderFeedback[i] = 0;
    encoderValueToROS[i] = 0;
    encoderFeedbackPreviousState[i] = encoderFeedback[i];
  }
}

void getFeedbackFromMotors() {
  for(int i = 0; i < 8; i++) {
    tcaselect(i); //INSERT YOUR NUMBER HERE IF YOU WOULD LIKE TO REQUEST A CERTAIN ENCODER INSTEAD OF ALL OF THEM 
    encoderFeedback[i] = encoderList[i].getRawPosition(); //max int 32,767, looks like encoder max is 16-bit value
    //same encoderFeedbackFromPreviousState holds max same number
    
    //for the reason of robot being mirrored left-to-right etc, left encoders show opposite direction, encoders 0 and 4 must be inverted
    if (i == 0) { //FRONT LEFT WHEEL
      encoderValueToROS[i] -= encoderFeedback[i];     
    } else if (i == 4) { //BACK LEFT WHEEL
      encoderValueToROS[i] -= encoderFeedback[i];     
    } else if (i == 3) { //FRONT RIGHT WHEEL
      encoderValueToROS[i] += encoderFeedback[i];     
    } else if (i == 7) { //BACK RIGHT WHEEL
      encoderValueToROS[i] += encoderFeedback[i];     
    }

    encoderList[i].zero();
  } //for loop for all 8 encoders
}

void publishEncoderValuesToROS() {
  //front encoders
  FL_msg.data = encoderValueToROS[0]; 
  FL_debug.publish(&FL_msg);

  FR_msg.data = encoderValueToROS[3]; 
  FR_debug.publish(&FR_msg);

  //rear encoders
  BL_msg.data = encoderValueToROS[4]; 
  BL_debug.publish(&BL_msg); 

  BR_msg.data = encoderValueToROS[7]; 
  BR_debug.publish(&BR_msg); 
}

//4 functions below perform data processing for the four separate speeds
void FRwheel(const std_msgs::Float64& msg1) {
  float FRvelocityFromROS = msg1.data;
  FRmotorSpeed = 255*(FRvelocityFromROS/maxAngularVelocity);
}

void FLwheel(const std_msgs::Float64& msg2) {
  float FLvelocityFromROS = msg2.data;
  FLmotorSpeed = 255*(FLvelocityFromROS/maxAngularVelocity);
}

void BLwheel(const std_msgs::Float64& msg3) {
  float BLvelocityFromROS = msg3.data;
  BLmotorSpeed = 255*(BLvelocityFromROS/maxAngularVelocity);
}

void BRwheel(const std_msgs::Float64& msg4) {
  float BRvelocityFromROS = msg4.data;
  BRmotorSpeed = 255*(BRvelocityFromROS/maxAngularVelocity); 
}

//main moving function
void moving() {
  //push power via contactor -> temporary
  digitalWrite(powerLocker, LOW); //LOW turns it on

  //execute
  FLmotor.go(BLmotorSpeed);
  FRmotor.go(BRmotorSpeed);
  BLmotor.go(BLmotorSpeed);
  BRmotor.go(BRmotorSpeed);
}

//Contactor power unlocker
void unlockPowerToMotors(const std_msgs::UInt16& cmd_msg) {
  if(cmd_msg.data == 0) {
    digitalWrite(powerLocker, HIGH); //HIGH turns it off
    // powerLocker_msg.data = "POWER IS OFF";
  }

  else if (cmd_msg.data == 1) {
    digitalWrite(powerLocker, LOW); //LOW turns it on
    // powerLocker_msg.data = "POWER IS ON";
  }
}