#ifndef config_h
#define config_h

#include "Arduino.h"

//i2c address
#define TCAADDR 0x70 //i2c mux address

//ethernet interface shield
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192, 168, 1, 1);

// Set the rosserial socket server IP address
IPAddress server_ip(192, 168, 1, 2);
// Set the rosserial socket server port
const uint16_t serverPort = 11411;

//encoder settings
double encoderResolution = 627.2; //627.2 for High Torque Conf, 392 for High Speed conf, 261.3 for Turbo Gear conf

//gear ratio
double gearRatio = 1.3; //reduction ratio

//////////////////////////////////////

#endif