#ifndef pinout_h
#define pinout_h

#include "Arduino.h"

//motor pinout
//Front Left Motor
int FLmotorIN1 = 33;
int FLmotorIN2 = 31;
int FLmotorPWM = 4;

//Front Right Motor
int FRmotorIN1 = 30;
int FRmotorIN2 = 32;
int FRmotorPWM = 3;

//Back Left Motor
int BLmotorIN1 = 36;
int BLmotorIN2 = 34;
int BLmotorPWM = 5;

//Back Right Motor
int BRmotorIN1 = 37;
int BRmotorIN2 = 35;
int BRmotorPWM = 6;

///////////////////////////////////////
//high current control
int powerLocker = 7; //unlocks power from the relay

//////////////////////////////////////

#endif