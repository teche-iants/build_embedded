//general interface
#include <Arduino.h>
#include <SPI.h>
#include <Ethernet.h>

// To use the TCP version of rosserial_arduino
#define ROSSERIAL_ARDUINO_TCP

//ROS
#include <ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Empty.h>


//power interface
#include <Motor.h>

//Feedback interface
#include <Wire.h>
#include <I2CEncoder.h> //https://github.com/alexhenning/I2CEncoder?fbclid=IwAR1FRFiVtNuNwpy7uDR8T3gacyD7tOC_vkBq7BlZtldRjwZJgsAbo6WTmyw
#define TCAADDR 0x70 //i2c mux address


////////////////////////////////////////Ethernet/////////////////////////////////
// Set the shield settings
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192, 168, 1, 1);

// Set the rosserial socket server IP address
IPAddress server(192, 168, 1, 2);
// Set the rosserial socket server port
const uint16_t serverPort = 11411;
uint16_t period = 20;
uint32_t last_time = 0;
////////////////////////////////////////////////////////////////////////////////

//motor control pins
//digital pins can be any digital pins
//PWM: 2 to 13 and 44 to 46. Provide 8-bit PWM output with the analogWrite() function.
//for documentation refer to the https://store.arduino.cc/usa/mega-2560-r3

//Project details: http://24.86.84.132:8090/pages/viewpage.action?pageId=7274634

/*PINOUT 
MOTOR CONTROLS
(outside of the enclosure view, left-to-right)
(inside view is right-to-left)
1---------------------------------------------------10: 
5V, PWM11, IN11, IN12, GND, GND, IN13, IN14, PWM12, 5V

11--------------------------------------------------20:
5V, PWM21, IN21, IN22, GND, GND, IN23, IN24, PWM22, 5V

ENCODERS FRONT/BACK
(outside of the enclosure view, left-to-right)
(inside view is right-to-left)
9-------------------------------------16:
5V, SDA3, SCL3, GND, GND, SDA4, SCL4, 5V

1-------------------------------------8: 
5V, SDA1, SCL1, GND, GND, SDA2, SCL2, 5V
 */

//motor variables + encoders (encoders use interrupt pins)
//Motor 1 - FR; Motor 2 - FL; Motor 3 - RL, Motor 4 - RR
float FLmotorSpeed = 0;
float FRmotorSpeed = 0;
float BLmotorSpeed = 0;
float BRmotorSpeed = 0;

unsigned long FLtime = 0;
unsigned long FRtime = 0;
unsigned long BLtime = 0;
unsigned long BRtime = 0;

double encoderResolution = 627.2; //627.2 for High Torque Conf, 392 for High Speed conf, 261.3 for Turbo Gear conf
double gearRatio = 1.3; //reduction ratio

uint32_t FLinitValueForSpeedCalculation = 0;
uint32_t FRinitValueForSpeedCalculation = 0;
uint32_t BLinitValueForSpeedCalculation = 0;
uint32_t BRinitValueForSpeedCalculation = 0;

//motors use /base/xx_wheel_pid_cmd where XX is the motor name FR, FL etc
/*
IF USING iANTS_DCU_mk.1, pinout is the following:
Updated pinout:
Bottom row (FRONT):
PWM3 = PWM4
IN11 = 31
IN12 = 33
PWM4 = PWM3
IN13 = 30
IN14 = 32

Top row (BACK):
PWM5 =  PWM5
IN21 = 34
IN22 = 36

PWM6 = PWM6
IN23 = 35
IN24 = 37
*/

Motor FLmotor = Motor(33, 31, 4); //FL
Motor FRmotor = Motor(30, 32, 3); //FR
Motor BLmotor = Motor(36, 34, 5); //RL
Motor BRmotor = Motor(37, 35, 6); //RR

/*
IF USING iANTS_DCU_mk.2
Motor FLmotor = Motor(31, 33, 3); //FL
Motor FRmotor = Motor(30, 32, 4); //FR
Motor BLmotor = Motor(35, 37, 5); //RL
Motor BRmotor = Motor(34, 36, 6); //RR
*/

//Encoder data
I2CEncoder* encoderList = new I2CEncoder[8]; //library for the 8-bit vex encoders
long* encoderFeedback = new long[8]; //encoder feedback from encoders uses LONG for the data type
long* encoderFeedbackPreviousState = new long[8];
int numberOfEncoderOverflows = 0;
uint32_t* encoderValueToROS = new uint32_t[8]; //these two arrays hold large numbers needed for the proper operation of the final feedback
uint32_t* encoderValueSentToROSFromPreviousState = new uint32_t[8];

//Motor debugger:
String rosString = "";
char* speed = new char[10];

//DCU specific function
void initializeEncoders();
void getFeedbackFromMotors();
void publishEncoderValuesToROS();
void publishWheelFeedbackToROS();
void FRwheel(const std_msgs::Float64& msg1);
void FLwheel(const std_msgs::Float64& msg2);
void BRwheel(const std_msgs::Float64& msg3);
void BLwheel(const std_msgs::Float64& msg4);
void moving();
void feedbackPIDMode();
void postFeedbackToROS();
void unlockPowerToMotors(const std_msgs::UInt16& cmd_msg);

//DCU variables
#define powerLocker 7 //unlocks power from the relay

////////////////////////////////////////////////ROS ////////////////////////////////////////////////////////////////////////////
//HANDLER
ros::NodeHandle DCU;

//DRIVE & POWERTRAIN -> speed is in rad/s
//NOTE: USE EITHER DIRECT COMMAND VELOCITY OR PID BASED CONTROL EFFORT
float maxAngularVelocity = 39.27; //39.27 rad/s max speed -> 1.5 m/s -> 29 rad/s with 2 inch wheel diameter -> 1.33 gear reduction

//direct values from cmd_vel
ros::Subscriber<std_msgs::Float64> FRwheelVelocity("/base/fr_wheel/cmd_vel", FRwheel); //FR wheel 
ros::Subscriber<std_msgs::Float64> FLwheelVelocity("/base/fl_wheel/cmd_vel", FLwheel); //FL wheel
ros::Subscriber<std_msgs::Float64> BRwheelVelocity("/base/br_wheel/cmd_vel", BRwheel); //BR wheel 
ros::Subscriber<std_msgs::Float64> BLwheelVelocity("/base/bl_wheel/cmd_vel", BLwheel); //BL wheel 

//TEST TOPIC - RE-WRITE IT MANUALLY TO TIGHT IT TO THE CERTAIN TOPIC
//ros::Subscriber<std_msgs::Float64> wheelTrigger("/base/bl_wheel/cmd_vel", BLwheel); //change wheel here to BL, BR, FR, FL

//contactor power
ros::Subscriber<std_msgs::UInt16> sub2("/base/contactor", unlockPowerToMotors);

//publish
std_msgs::String powerLocker_msg;
ros::Publisher motor_power("motor_debug", &powerLocker_msg);

//encoders to ROS support
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Front Encoders (REFER TO THE ALTIUM SCHEMATIC FOR DETAILS!!!!):
Encoder1 -> Front Left Top                                      Encoder4 -> Front Right Top
Encoder2 -> Front Left Bottom                                   Encoder3 -> Front Right Bottom

Rear Encoders:
Encoder5 -> Rear Left Top                                       Encoder8 -> Rear Right Top
Encoder6 -> Rear Left Bottom                                    Encoder7 -> Rear Right Bottom
*/ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

std_msgs::UInt32 FR_msg; //Front Right top wheel
ros::Publisher FR_debug("/base/fr_wheel/encoder", &FR_msg);

std_msgs::UInt32 FL_msg; //Front Right top wheel
ros::Publisher FL_debug("/base/fl_wheel/encoder", &FL_msg);

std_msgs::UInt32 BR_msg; //Front Right top wheel
ros::Publisher BR_debug("/base/br_wheel/encoder", &BR_msg);

std_msgs::UInt32 BL_msg; //Front Right top wheel
ros::Publisher BL_debug("/base/bl_wheel/encoder", &BL_msg);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
Wheel speed feedback:
Algorithm: take microcontroller clock-cycle based timing and calculate average change in encoder reading per second. Then calculate actual
linear and angular velocity of the wheel based on the radius
*/
std_msgs::UInt32 FRvelocity; //Rear Left top wheel
ros::Publisher FRfeedbackVelocity("/base/fr_wheel/embedded_feedback", &FRvelocity);

std_msgs::UInt32 FLvelocity; //Rear Left top wheel
ros::Publisher FLfeedbackVelocity("/base/fl_wheel/embedded_feedback", &FLvelocity);

std_msgs::UInt32 BRvelocity; //Rear Left top wheel
ros::Publisher BRfeedbackVelocity("/base/br_wheel/embedded_feedback", &BRvelocity);

std_msgs::UInt32 BLvelocity; //Rear Left top wheel
ros::Publisher BLfeedbackVelocity("/base/bl_wheel/embedded_feedback", &BLvelocity);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//MAIN
void setup() {
  // Use serial to monitor the process
  Serial.begin(9600);
  Wire.begin();
  // Connect the Ethernet
  Ethernet.begin(mac, ip);
  // Let some time for the Ethernet Shield to be initialized
  delay(1000);

  Serial.println("");
  Serial.println("MCU Ethernet connected");
  Serial.println("IP address: ");
  Serial.println(Ethernet.localIP());

  // Set the connection to rosserial socket server
  DCU.getHardware()->setConnection(server, serverPort);
  //ros
  DCU.initNode();

  // Another way to get IP
  Serial.print("IP = ");
  Serial.println(DCU.getHardware()->getLocalIP());

  //motor subs
  DCU.subscribe(FRwheelVelocity);
  DCU.subscribe(FLwheelVelocity);
  DCU.subscribe(BRwheelVelocity);
  DCU.subscribe(BLwheelVelocity);
  //DCU.subscribe(wheelTrigger); //universal wheel tester for Float64 speed tests

  //contactor sub
  DCU.subscribe(sub2);

  //contactor pub
  DCU.advertise(motor_power);

  //encoder pub
  DCU.advertise(FR_debug);
  DCU.advertise(FL_debug);
  DCU.advertise(BR_debug);
  DCU.advertise(BL_debug);

  DCU.advertise(FRfeedbackVelocity);
  DCU.advertise(FLfeedbackVelocity);
  DCU.advertise(BRfeedbackVelocity);
  DCU.advertise(BLfeedbackVelocity);

  //INITIALIZATION OF HARDWARE
  initializeEncoders(); //constant operation
  pinMode(powerLocker, OUTPUT);
}

void loop() {
    if(millis() - last_time >= period)
  {
    last_time = millis();
    if (DCU.connected())
    {
      Serial.println("Connected");
      // topic publishers
      motor_power.publish( &powerLocker_msg ); //power locker, str_msg.data is generated based on the input from the ROS power locker 

      getFeedbackFromMotors(); //get feedback from all available encoders
      publishEncoderValuesToROS(); //publish values to ROS

      //execute
      moving();
      } else {
      Serial.println("Not Connected");
    }
  }
  //node handling
  DCU.spinOnce();
  delay(1);
}


/////////////////////////////////////////////////////////////FUNCTIONS //////////////////////////////////////////////////////////////////////////////////////////////
void tcaselect(uint8_t i) { //8 encoders multiplexing
  if (i > 7) return;
 
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();  
}

void initializeEncoders() {
  Wire.begin();

  for(int i = 0; i < 8; i++) {
    tcaselect(i); //INSERT YOUR NUMBER HERE IF YOU WOULD LIKE TO REQUEST A CERTAIN ENCODER INSTEAD OF ALL OF THEM
    encoderList[i].init(MOTOR_393_TORQUE_ROTATIONS, MOTOR_393_TIME_DELTA);
    encoderFeedback[i] = 0;
    encoderValueToROS[i] = 0;
    encoderFeedbackPreviousState[i] = encoderFeedback[i];
  }
}

void getFeedbackFromMotors() {
  for(int i = 0; i < 8; i++) {
    tcaselect(i); //INSERT YOUR NUMBER HERE IF YOU WOULD LIKE TO REQUEST A CERTAIN ENCODER INSTEAD OF ALL OF THEM 
    encoderFeedback[i] = encoderList[i].getRawPosition(); //max int 32,767, looks like encoder max is 16-bit value
    //same encoderFeedbackFromPreviousState holds max same number

    //for the reason of robot being mirrored left-to-right etc, left encoders show opposite direction, encoders 0 and 4 must be inverted
    if(i==0 || i == 4) { //0 - FL, 4 - BL, 3 - FR, 7 - BR
      encoderValueToROS[i] -= encoderFeedback[i];
      
      //speed calculation algorithm
      if (BLinitValueForSpeedCalculation != 0) {
        uint32_t BLdeltaRotations = (encoderValueToROS[4] - BLinitValueForSpeedCalculation)/encoderResolution;
        BLmotorSpeed = 0.769*2*3.14*(1000000*BLdeltaRotations/(millis() - BLtime)); //0.769 = gear reduction 1/1.3; 2*pi*RPS/60 = rad/s
        BLinitValueForSpeedCalculation = encoderValueToROS[4];
        BLtime = millis();
      } else if (FLinitValueForSpeedCalculation != 0) {
        uint32_t FLdeltaRotations = (encoderValueToROS[0] - FLinitValueForSpeedCalculation)/encoderResolution;
        FLmotorSpeed = 0.769*2*3.14*(1000000*FLdeltaRotations/(millis() - FLtime)); //0.769 = gear reduction 1/1.3; 2*pi*RPS/60 = rad/s        
        FLinitValueForSpeedCalculation = encoderValueToROS[0];
        FLtime = millis();
      } else if (FLinitValueForSpeedCalculation == 0 || BLinitValueForSpeedCalculation == 0) {
        if (FLinitValueForSpeedCalculation == 0) {
          FLinitValueForSpeedCalculation = encoderValueToROS[0];
          FLtime = millis();
        } else if (BLinitValueForSpeedCalculation == 0) {
          BLinitValueForSpeedCalculation = encoderValueToROS[4];        
          BLtime = millis();
        }
      }
      ////end of speed calculation
    
    } else if (i == 3 || i == 7) { //0 - FL, 4 - BL, 3 - FR, 7 - BR
      encoderValueToROS[i] += encoderFeedback[i];  

      //speed calculation algorithm
      if (FRinitValueForSpeedCalculation != 0) {
        uint32_t FRdeltaRotations = (encoderValueToROS[3] - FRinitValueForSpeedCalculation)/encoderResolution;
        FRmotorSpeed = 0.769*2*3.14*(1000000*FRdeltaRotations/(millis() - FRtime)); //0.769 = gear reduction 1/1.3; 2*pi*RPS/60 = rad/s
        FRinitValueForSpeedCalculation = encoderValueToROS[3];
        FRtime = millis();
      } else if (BRinitValueForSpeedCalculation != 0) {
        uint32_t BRdeltaRotations = (encoderValueToROS[7] - BRinitValueForSpeedCalculation)/encoderResolution;
        BRmotorSpeed = 0.769*2*3.14*(1000000*BRdeltaRotations/(millis() - BRtime)); //0.769 = gear reduction 1/1.3; 2*pi*RPS/60 = rad/s        
        BRinitValueForSpeedCalculation = encoderValueToROS[7];
        BRtime = millis();
      } else if (BRinitValueForSpeedCalculation == 0 || FRinitValueForSpeedCalculation == 0) {
        if (FRinitValueForSpeedCalculation == 0) {
          FRinitValueForSpeedCalculation = encoderValueToROS[3];
          FRtime = millis();
        } else if (BRinitValueForSpeedCalculation == 0) {
          BRinitValueForSpeedCalculation = encoderValueToROS[7];        
          BRtime = millis();
        }
      }
      ////end of speed calculation
    }

    encoderList[i].zero();
  } //for loop for all 8 encoders
}

void publishEncoderValuesToROS() {
  FLvelocity.data = FLmotorSpeed; 
  FLfeedbackVelocity.publish(&FLvelocity);

  FRvelocity.data = FRmotorSpeed; 
  FRfeedbackVelocity.publish(&FRvelocity);

  BLvelocity.data = BLmotorSpeed; 
  BLfeedbackVelocity.publish(&BLvelocity);

  BRvelocity.data = BRmotorSpeed; 
  BRfeedbackVelocity.publish(&BRvelocity);
}

void publishWheelFeedbackToROS() {
  //front encoders
  FL_msg.data = encoderValueToROS[0]; 
  FL_debug.publish(&FL_msg);

  FR_msg.data = encoderValueToROS[3]; 
  FR_debug.publish(&FR_msg);

  // FLt_msg.data = encoderValueToROS[0]; //encoder number 1-1 = 0
  // FLt_debug.publish(&FLt_msg); //Front Left Top encoder

  // FLb_msg.data = encoderValueToROS[1]; //encoder number 2-1 = 1
  // FLb_debug.publish(&FLb_msg); //Front Left Bottom encoder

  // FRb_msg.data = encoderValueToROS[2]; //encoder number 3 -1 = 2 because encoder numbers in I2C bus start with 0
  // FRb_debug.publish(&FRb_msg); //Front Right Bottom encoder

  //FRt_msg.data = encoderValueToROS[3]; //encoder number 4 -1 = 3 because encoder numbers in I2C bus start with 0
  //FRt_debug.publish(&FRt_msg); //Front Right top encoder

  //rear encoders
  BL_msg.data = encoderValueToROS[4]; 
  BL_debug.publish(&BL_msg); 

  BR_msg.data = encoderValueToROS[7]; 
  BR_debug.publish(&BR_msg); 

  // RLt_msg.data = encoderValueToROS[4]; //encoder number 5-1 = 4
  // RLt_debug.publish(&RLt_msg); //Rear Left Top encoder

  // RLb_msg.data = encoderValueToROS[5]; //encoder number 6-1 = 5
  // FLb_debug.publish(&RLb_msg); //Rear Left Bottom encoder

  // RRb_msg.data = encoderValueToROS[6]; //encoder number 7 -1 = 6 because encoder numbers in I2C bus start with 0
  // RRb_debug.publish(&RRb_msg); //Rear Right Bottom encoder

  // RRt_msg.data = encoderValueToROS[7]; //encoder number 8 -1 = 7 because encoder numbers in I2C bus start with 0
  // RRt_debug.publish(&RRt_msg); //Rear Right top encoder
}

//4 functions below perform data processing for the four separate speeds
void FRwheel(const std_msgs::Float64& msg1) {
  float FRvelocityFromROS = msg1.data;
  FRmotorSpeed = 255*(FRvelocityFromROS/maxAngularVelocity);
}

void FLwheel(const std_msgs::Float64& msg2) {
  float FLvelocityFromROS = msg2.data;
  FLmotorSpeed = 255*(FLvelocityFromROS/maxAngularVelocity);
}

void BLwheel(const std_msgs::Float64& msg3) {
  float BLvelocityFromROS = msg3.data;
  BLmotorSpeed = 255*(BLvelocityFromROS/maxAngularVelocity);
}

void BRwheel(const std_msgs::Float64& msg4) {
  float BRvelocityFromROS = msg4.data;
  BRmotorSpeed = 255*(BRvelocityFromROS/maxAngularVelocity); 
}

//main moving function
void moving() {
  //push power via contactor -> temporary
  digitalWrite(powerLocker, LOW); //LOW turns it on

  //debug, comment for faster operation
  //Serial.print(FLmotorSpeed); Serial.print(", "); Serial.print(FRmotorSpeed); Serial.print(", "); Serial.print(BLmotorSpeed); Serial.print(", "); Serial.println(BRmotorSpeed);

  //execute
  FLmotor.go(FLmotorSpeed);
  FRmotor.go(FRmotorSpeed);
  BLmotor.go(BLmotorSpeed);
  BRmotor.go(BRmotorSpeed);
}

//Contactor power unlocker
void unlockPowerToMotors(const std_msgs::UInt16& cmd_msg) {
  if(cmd_msg.data == 0) {
    digitalWrite(powerLocker, HIGH); //HIGH turns it off
    powerLocker_msg.data = "POWER IS OFF";
  }

  else if (cmd_msg.data == 1) {
    digitalWrite(powerLocker, LOW); //LOW turns it on
    powerLocker_msg.data = "POWER IS ON";
  }
}